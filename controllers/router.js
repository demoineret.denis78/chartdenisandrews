const express = require('express');
const router = express.Router();
const puppeteer = require('puppeteer');

router.get("/", async (req, res) => {
  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('https://olympics-statistics.com/nations');

    // Extraction des noms et liens des athlètes avec Puppeteer
    const countriesData = await page.evaluate(() => {
      const countriesList = document.querySelectorAll('a.card.nation.visible');
      const data = [];
      let count = 0; // Compteur pour suivre le nombre de pays ajoutés
      countriesList.forEach(country => {
        if (count < 3) { // Limiter à 10 pays
          const nameElement = country.querySelector('.bez');
          const name = nameElement.textContent.trim();
          const link = country.getAttribute('href');
          data.push({ name, link });
          count++; // Incrémenter le compteur
        } else {
          return data; // Si on a déjà ajouté 10 pays, retourner les données actuelles
        }
      });
      return data;
    });

    // Récupération des informations sur les médailles pour chaque pays
    for (const country of countriesData) {
      // Accéder à la page spécifique du pays
      const countryPage = await browser.newPage();
      await countryPage.goto(`https://olympics-statistics.com${country.link}`);

      // Récupération des informations sur les médailles à partir de la page du pays
      const countryMedals = await countryPage.evaluate(() => {
        const medalDivs = Array.from(document.querySelectorAll('div.rnd.teaser > div'));

        let goldMedals = 0;
        let silverMedals = 0;
        let bronzeMedals = 0;

        medalDivs.filter(div => div.querySelector('.the-medal')).forEach(div => {
          const medalType = div.querySelector('.the-medal').getAttribute('data-medal');
          const medals = parseInt(div.querySelector('span.mal').textContent.trim());

          if (medalType === '1') {
            goldMedals += medals;
          } else if (medalType === '2') {
            silverMedals += medals;
          } else if (medalType === '3') {
            bronzeMedals += medals;
          }
        });

        return { gold: goldMedals, silver: silverMedals, bronze: bronzeMedals };
      });

      // Ajout des informations sur les médailles au dictionnaire de données du pays
      country.medals = countryMedals;

      // Fermer la page du pays
      await countryPage.close();
    }

    console.log('Countries Data with Medals:', countriesData);






    const browser2 = await puppeteer.launch();
    const page2 = await browser2.newPage();
    await page2.goto('https://olympics-statistics.com/olympic-athletes');

    // Extraction des noms et liens des athlètes avec Puppeteer
    const athletesData = await page2.evaluate(() => {
      const athletesList = document.querySelectorAll('a.card.athlet.visible');
      const data2 = [];
      let count = 0; // Compteur pour suivre le nombre d'athlètes ajoutés
      athletesList.forEach(athlete => {
        if (count < 3) { // Limiter à 10 athlètes
          const nameElement = athlete.querySelector('.vn');
          const name = nameElement.textContent.trim();
          const link = athlete.getAttribute('href');
          data2.push({ name, link });
          count++; // Incrémenter le compteur
        }
      });
      return data2;
    });


    // Récupération des informations sur les médailles pour chaque athlète
    for (const athlete of athletesData) {
      // Accéder à la page spécifique de l'athlète
      const athletePage = await browser2.newPage();
      await athletePage.goto(`https://olympics-statistics.com/${athlete.link}`);
      console.log(`https://olympics-statistics.com/${athlete.link}`);
      // Récupération des informations sur les médailles à partir de la page de l'athlète
      const athleteMedals = await athletePage.evaluate(() => {
        // Sélectionne tous les éléments div avec la classe "medaille visible"
        const medailles = document.querySelectorAll('div.medaille.visible');

        console.log(medailles);
        // Compte le nombre d'éléments sélectionnés
        var nombreDeMedailles = medailles.length;

        // Affiche le résultat
        console.log("Nombre de médailles visibles :", nombreDeMedailles);
        return nombreDeMedailles;
        // athlete.medals = nombreDeMedailles;
      });

      athlete.medals = athleteMedals;
      console.log("Nombre de médailles visibles :", athleteMedals);
      // Ajout des informations sur les médailles à l'objet de données de l'athlète


      // Fermer la page de l'athlète
      await athletePage.close();
    }




    // Récupération des liens et noms des sports olympiques
    const sportsPage = await browser.newPage();
    await sportsPage.goto('https://olympics-statistics.com/olympic-sports');

    const sportsData = await sportsPage.evaluate(() => {
      const sportsList = document.querySelectorAll('a.card.sport.visible');
      const data = [];
      let count = 0; // Limite à 10 sports
      sportsList.forEach(sport => {
        if (count < 3) {
          const nameElement = sport.querySelector('.bez');
          const name = nameElement.textContent.trim();
          const link = sport.getAttribute('href');
          data.push({ name, link });
          count++; // Incrémente le compteur
        }
      });
      return data;
    });

    // Fermer la page des sports olympiques
    await sportsPage.close();

    // Récupération des informations sur les médailles pour chaque sport
    const sportMedalsData = [];

    for (const sport of sportsData) {
      const sportPage = await browser.newPage();
      await sportPage.goto(`https://olympics-statistics.com${sport.link}`);

      const medalsData = await sportPage.evaluate(() => {
        const countriesList = document.querySelectorAll('div.card.nation.visible');
        const medals = {};

        countriesList.forEach(country => {
          const countryName = country.querySelector('.bez .n').textContent.trim();
          const totalMedals = parseInt(country.querySelector('.bez .mal').textContent.trim());
          const goldMedals = parseInt(country.querySelector('.the-medal[data-medal="1"]').parentNode.textContent.trim());
          const silverMedals = parseInt(country.querySelector('.the-medal[data-medal="2"]').parentNode.textContent.trim());
          const bronzeMedals = parseInt(country.querySelector('.the-medal[data-medal="3"]').parentNode.textContent.trim());

          medals[countryName] = { total: totalMedals, gold: goldMedals, silver: silverMedals, bronze: bronzeMedals };
        });

        return medals;
      });

      sportMedalsData.push({ name: sport.name, medals: medalsData });

      await sportPage.close();
    }

    // Affichage des données récupérées
    console.log('Sport Medals Data:', sportMedalsData);


    // Rendre la vue EJS avec les données des pays
    res.render("index", { countriesData, athletesData, sportMedalsData });



    // Fermer le navigateur Puppeteer
    await browser.close();
    await browser2.close();
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Internal Server Error");
  }
});










































router.get("/chart", async (req, res) => {
  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('https://olympics-statistics.com/nations');

    // Extraction des noms et liens des athlètes avec Puppeteer
    const countriesData = await page.evaluate(() => {
      const countriesList = document.querySelectorAll('a.card.nation.visible');
      const data = [];
      let count = 0; // Compteur pour suivre le nombre de pays ajoutés
      countriesList.forEach(country => {
        if (count < 3) { // Limiter à 10 pays
          const nameElement = country.querySelector('.bez');
          const name = nameElement.textContent.trim();
          const link = country.getAttribute('href');
          data.push({ name, link });
          count++; // Incrémenter le compteur
        } else {
          return data; // Si on a déjà ajouté 10 pays, retourner les données actuelles
        }
      });
      return data;
    });

    // Récupération des informations sur les médailles pour chaque pays
    for (const country of countriesData) {
      // Accéder à la page spécifique du pays
      const countryPage = await browser.newPage();
      await countryPage.goto(`https://olympics-statistics.com${country.link}`);

      // Récupération des informations sur les médailles à partir de la page du pays
      const countryMedals = await countryPage.evaluate(() => {
        const medalDivs = Array.from(document.querySelectorAll('div.rnd.teaser > div'));

        let goldMedals = 0;
        let silverMedals = 0;
        let bronzeMedals = 0;

        medalDivs.filter(div => div.querySelector('.the-medal')).forEach(div => {
          const medalType = div.querySelector('.the-medal').getAttribute('data-medal');
          const medals = parseInt(div.querySelector('span.mal').textContent.trim());

          if (medalType === '1') {
            goldMedals += medals;
          } else if (medalType === '2') {
            silverMedals += medals;
          } else if (medalType === '3') {
            bronzeMedals += medals;
          }
        });

        return { gold: goldMedals, silver: silverMedals, bronze: bronzeMedals };
      });

      // Ajout des informations sur les médailles au dictionnaire de données du pays
      country.medals = countryMedals;

      // Fermer la page du pays
      await countryPage.close();
    }

    console.log('Countries Data with Medals:', countriesData);






    const browser2 = await puppeteer.launch();
    const page2 = await browser2.newPage();
    await page2.goto('https://olympics-statistics.com/olympic-athletes');

    // Extraction des noms et liens des athlètes avec Puppeteer
    const athletesData = await page2.evaluate(() => {
      const athletesList = document.querySelectorAll('a.card.athlet.visible');
      const data2 = [];
      let count = 0; // Compteur pour suivre le nombre d'athlètes ajoutés
      athletesList.forEach(athlete => {
        if (count < 3) { // Limiter à 10 athlètes
          const nameElement = athlete.querySelector('.vn');
          const name = nameElement.textContent.trim();
          const link = athlete.getAttribute('href');
          data2.push({ name, link });
          count++; // Incrémenter le compteur
        }
      });
      return data2;
    });


    // Récupération des informations sur les médailles pour chaque athlète
    for (const athlete of athletesData) {
      // Accéder à la page spécifique de l'athlète
      const athletePage = await browser2.newPage();
      await athletePage.goto(`https://olympics-statistics.com/${athlete.link}`);
      console.log(`https://olympics-statistics.com/${athlete.link}`);
      // Récupération des informations sur les médailles à partir de la page de l'athlète
      const athleteMedals = await athletePage.evaluate(() => {
        // Sélectionne tous les éléments div avec la classe "medaille visible"
        const medailles = document.querySelectorAll('div.medaille.visible');

        console.log(medailles);
        // Compte le nombre d'éléments sélectionnés
        var nombreDeMedailles = medailles.length;

        // Affiche le résultat
        console.log("Nombre de médailles visibles :", nombreDeMedailles);
        return nombreDeMedailles;
        // athlete.medals = nombreDeMedailles;
      });

      athlete.medals = athleteMedals;
      console.log("Nombre de médailles visibles :", athleteMedals);
      // Ajout des informations sur les médailles à l'objet de données de l'athlète


      // Fermer la page de l'athlète
      await athletePage.close();
    }





     // Récupération des liens et noms des sports olympiques
     const sportsPage = await browser.newPage();
     await sportsPage.goto('https://olympics-statistics.com/olympic-sports');
 
     const sportsData = await sportsPage.evaluate(() => {
       const sportsList = document.querySelectorAll('a.card.sport.visible');
       const data = [];
       let count = 0; // Limite à 10 sports
       sportsList.forEach(sport => {
         if (count < 3) {
           const nameElement = sport.querySelector('.bez');
           const name = nameElement.textContent.trim();
           const link = sport.getAttribute('href');
           data.push({ name, link });
           count++; // Incrémente le compteur
         }
       });
       return data;
     });
 
     // Fermer la page des sports olympiques
     await sportsPage.close();
 
     // Récupération des informations sur les médailles pour chaque sport
     const sportMedalsData = [];
 
     for (const sport of sportsData) {
       const sportPage = await browser.newPage();
       await sportPage.goto(`https://olympics-statistics.com${sport.link}`);
 
       const medalsData = await sportPage.evaluate(() => {
         const countriesList = document.querySelectorAll('div.card.nation.visible');
         const medals = {};
 
         countriesList.forEach(country => {
           const countryName = country.querySelector('.bez .n').textContent.trim();
           const totalMedals = parseInt(country.querySelector('.bez .mal').textContent.trim());
           const goldMedals = parseInt(country.querySelector('.the-medal[data-medal="1"]').parentNode.textContent.trim());
           const silverMedals = parseInt(country.querySelector('.the-medal[data-medal="2"]').parentNode.textContent.trim());
           const bronzeMedals = parseInt(country.querySelector('.the-medal[data-medal="3"]').parentNode.textContent.trim());
 
           medals[countryName] = { total: totalMedals, gold: goldMedals, silver: silverMedals, bronze: bronzeMedals };
         });
 
         return medals;
       });
 
       sportMedalsData.push({ name: sport.name, medals: medalsData });
 
       await sportPage.close();
     }
 
     // Affichage des données récupérées

    // Affichage des données récupérées
    console.log('Sport Medals Data:', sportMedalsData);


    // Rendre la vue EJS avec les données des pays
    res.render("chart", { countriesData, athletesData, sportMedalsData });



    // Fermer le navigateur Puppeteer
    await browser.close();
    await browser2.close();
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Internal Server Error");
  }
});




module.exports = router;
