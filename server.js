require('dotenv').config();
const express = require('express');
const app = express();
const routes = require('./controllers/router');
const path = require('path');

// dire a node que l'on utilise ejs comme moteur de template
app.set('view engine', 'ejs');
// definir le dossier des vues
app.set('views', path.join(__dirname, 'views'));
// déclarer le dossier des statiques
app.use(express.static(path.join(__dirname, 'publics')));

// Ajouter le middleware pour analyser les corps de requête en JSON
app.use(express.json());

// definir les routes du controller "router" sur les routes /
app.use('/', routes);

app.listen(process.env.PORT, () => {
  console.log(`Server started on port ${process.env.PORT}`);
});
